# Film Net Test

Date: July 16, 2022
Status: In Progress

# Project Documentation

## Technologies:

In this section we will explain what technology used to handle each part of project:

### Overall:

We use navigation component to have single activity application, we use viewModel to receive and store our data from network layer.

We have a SearchFragment which contains SearchView in that view we have an input and a recyclerView to show video list. List will update with a small debounce on user change input text.

### Network:

We use these following library for our network layer part:

- Retrofit
- OkHttp
- RxJava

### View:

We use Kotlin code to describe our view instead of XML.

### Image Loader:

We user this following library for load our image: 

- Picasso

### Dependency Injection:

We user this following library for handle DI: 

- Koin