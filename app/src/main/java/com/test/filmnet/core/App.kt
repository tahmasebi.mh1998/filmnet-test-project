package com.test.filmnet.core

import android.app.Application
import androidx.appcompat.app.AppCompatDelegate
import com.test.filmnet.di.repositoryModule
import com.test.filmnet.di.retrofitModule
import com.test.filmnet.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class App: Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger(Level.ERROR)
            androidContext(this@App)
            modules(listOf(retrofitModule, repositoryModule, viewModelModule))
        }
    }

}