package com.test.filmnet.network.model.video

data class VideoModel(
    val age_restriction: String,
    val alter_cover_image: Image,
    val categories: List<Category>,
    val conditional_flag: String,
    val cover_image: Image,
    val duration: String,
    val flag: String,
    val id: String,
    val imdb_rank_percent: Int,
    val original_name: String,
    val page_title: String,
    val poster_image: Image,
    val rate: Double,
    val short_id: String,
    val slug: String,
    val status: String,
    val summary: String,
    val title: String,
    val type: String,
    val year: Int
)