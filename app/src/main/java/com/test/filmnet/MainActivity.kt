package com.test.filmnet

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AppCompatDelegate
import androidx.navigation.fragment.NavHostFragment

class MainActivity : AppCompatActivity() {

    private var mainView: MainView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mainView = MainView(this)
        setContentView(mainView)

        configNavigationComponent()
    }

    private fun configNavigationComponent() {
        val finalHost = NavHostFragment.create(R.navigation.app_navigation)
        supportFragmentManager.beginTransaction()
            .replace(mainView?.id!!, finalHost)
            .setPrimaryNavigationFragment(finalHost) // equivalent to app:defaultNavHost="true"
            .commit()
    }

}