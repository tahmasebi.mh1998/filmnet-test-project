package com.test.filmnet.search.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.test.filmnet.network.model.Meta
import com.test.filmnet.network.model.Result
import com.test.filmnet.network.model.SearchData
import com.test.filmnet.network.model.video.VideoModel
import com.test.filmnet.network.repository.Repository
import io.reactivex.observers.DisposableSingleObserver
import java.util.*


class SearchViewModel(private val repository: Repository): ViewModel() {

    val liveData = MutableLiveData<List<VideoModel>?>()
    var lastMeta: Meta? = null

    private var requestId: String = ""

    init {
        //User will see a video list for first time open the app
        search("")
    }

    /*
    val localId: String = generateUUID()
    requestId = localId

    it use to handle concurrent request, Live data will pass just the last request user send.
    */


    fun search(query: String){
        val localId: String = generateUUID()
        requestId = localId
        val disposable = repository.search(query).subscribeWith(object : DisposableSingleObserver<Result<SearchData>>() {
            override fun onSuccess(t: Result<SearchData>) {
                if (requestId == localId){
                    liveData.value = t.data?.videos
                    lastMeta = t.meta
                }
            }

            override fun onError(e: Throwable) {
                Log.e(javaClass.name, "onError: $e")
            }

        })

    }

    fun getNextPage() {
        lastMeta?.let {
            if (it.nextUrl.isNullOrEmpty().not()){
                val localId: String = generateUUID()
                requestId = localId
                val disposable = repository.getNextPageWithUrl(it.nextUrl!!).subscribeWith(object : DisposableSingleObserver<Result<List<VideoModel>>>() {
                    override fun onSuccess(t: Result<List<VideoModel>>) {
                        if (requestId == localId){
                            liveData.value = t.data
                            lastMeta = t.meta
                        }
                    }

                    override fun onError(e: Throwable) {
                        Log.e(javaClass.name, "onError: $e")
                    }


                })
            }
        }
    }

    private fun generateUUID() = UUID.randomUUID().toString()

}