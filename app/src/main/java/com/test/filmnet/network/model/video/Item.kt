package com.test.filmnet.network.model.video

data class Item(
    val title: String
)