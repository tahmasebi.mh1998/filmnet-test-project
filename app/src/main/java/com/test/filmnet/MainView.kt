package com.test.filmnet

import android.content.Context
import android.widget.FrameLayout

class MainView(context: Context) : FrameLayout(context) {

    init {
        id = generateViewId()
    }
}