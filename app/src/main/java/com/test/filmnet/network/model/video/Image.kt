package com.test.filmnet.network.model.video

data class Image(
    val path: String
)