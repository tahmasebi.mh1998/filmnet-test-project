package com.test.filmnet.network.model

import com.google.gson.annotations.SerializedName
import com.test.filmnet.network.model.video.VideoModel

data class SearchData(
    @SerializedName("videos") val videos: List<VideoModel>?,
    @SerializedName("promoted_videos") val promotedVideos: List<VideoModel>?
)


