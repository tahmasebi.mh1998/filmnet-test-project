package com.test.filmnet.search

import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.text.TextUtils
import android.util.TypedValue
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.ListPopupWindow
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import com.squareup.picasso.Picasso
import com.test.filmnet.R
import com.test.filmnet.core.Utility
import com.test.filmnet.network.model.video.VideoModel

class VideoItemView(context: Context) : CardView(context) {

    private var constraintLayout: ConstraintLayout? = null
    private var imageCardView: CardView? = null
    private var imageView: AppCompatImageView? = null
    private var nameTextView: TextView? = null

    init {
        radius = Utility.dpToPx(context,12).toFloat()
        elevation = Utility.dpToPx(context, 4).toFloat()

        constraintLayout = ConstraintLayout(context).apply {

            imageCardView = CardView(context).apply {
                id = generateViewId()
                radius = Utility.dpToPx(context,12).toFloat()
                elevation = Utility.dpToPx(context, 0).toFloat()
                cardElevation = Utility.dpToPx(context, 0).toFloat()

                imageView = AppCompatImageView(context).apply {

                    scaleType = ImageView.ScaleType.CENTER_CROP
                }
                addView(
                    imageView,
                    FrameLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT
                    )
                )
            }
            addView(
                imageCardView,
                ConstraintLayout.LayoutParams(
                    0,
                    0
                ).apply {
                }
            )

            nameTextView = TextView(context).apply {
                id = generateViewId()
                setTextColor(Color.BLACK)
                typeface = Typeface.DEFAULT_BOLD
                setTextSize(TypedValue.COMPLEX_UNIT_DIP, 16f)
                setSingleLine()
                setLineSpacing(0f, 1.2f)
                ellipsize = TextUtils.TruncateAt.END
            }
            addView(
                nameTextView,
                ConstraintLayout.LayoutParams(
                    ListPopupWindow.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
                ).apply {
                }
            )
        }
        addView(
            constraintLayout,
            LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT).apply {

            }
        )

        setConstraint()
    }

    private fun setConstraint() {
        ConstraintSet().apply {
            clone(constraintLayout)

            //image card
            connect(imageCardView?.id!!, ConstraintSet.START, ConstraintSet.PARENT_ID, ConstraintSet.START, Utility.dpToPx(context, 12))
            connect(imageCardView?.id!!, ConstraintSet.TOP, ConstraintSet.PARENT_ID, ConstraintSet.TOP, Utility.dpToPx(context, 12))
            connect(imageCardView?.id!!, ConstraintSet.END, ConstraintSet.PARENT_ID, ConstraintSet.END, Utility.dpToPx(context, 12))
            setDimensionRatio(imageCardView?.id!!, "3:4")

            //name text view
            connect(nameTextView?.id!!, ConstraintSet.START, ConstraintSet.PARENT_ID, ConstraintSet.START, Utility.dpToPx(context, 12))
            connect(nameTextView?.id!!, ConstraintSet.TOP, imageCardView?.id!!, ConstraintSet.BOTTOM, Utility.dpToPx(context, 12))
            connect(nameTextView?.id!!, ConstraintSet.END, ConstraintSet.PARENT_ID, ConstraintSet.END, Utility.dpToPx(context, 12))
            connect(nameTextView?.id!!, ConstraintSet.BOTTOM, ConstraintSet.PARENT_ID, ConstraintSet.BOTTOM, Utility.dpToPx(context, 12))

            applyTo(constraintLayout)
        }
    }

    fun setupView(videoModel: VideoModel){
        Picasso.get().load(videoModel.poster_image.path).placeholder(R.drawable.placeholder).into(imageView)
        nameTextView?.text = videoModel.title
    }

}