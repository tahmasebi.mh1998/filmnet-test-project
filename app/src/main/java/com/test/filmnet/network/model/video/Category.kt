package com.test.filmnet.network.model.video

data class Category(
    val items: List<Item>,
    val type: String
)