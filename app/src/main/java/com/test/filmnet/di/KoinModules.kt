package com.test.filmnet.di

import com.test.filmnet.network.ApiService
import com.test.filmnet.network.RetrofitClient
import com.test.filmnet.network.repository.Repository
import com.test.filmnet.search.viewmodel.SearchViewModel
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import java.util.concurrent.TimeUnit


val viewModelModule = module {
    viewModel {
        SearchViewModel(get())
    }
}

val repositoryModule = module {
    single {
        Repository(get())
    }
}


val retrofitModule = module {
    factory { provideHttpClient() }
    factory { provideApi(get()) }
    single() { RetrofitClient(get()) }
}


fun provideApi(retrofitClient: RetrofitClient): ApiService {
    return retrofitClient.getClient().create(ApiService::class.java)
}

fun provideHttpClient(): OkHttpClient {
    val REQUEST_TIMEOUT: Long = TimeUnit.SECONDS.toMillis(30)
    return OkHttpClient.Builder()
        .connectTimeout(REQUEST_TIMEOUT, TimeUnit.MILLISECONDS)
        .readTimeout(REQUEST_TIMEOUT, TimeUnit.MILLISECONDS)
        .writeTimeout(REQUEST_TIMEOUT, TimeUnit.MILLISECONDS)
        .addInterceptor(Interceptor { chain ->
            val request =
                chain.request().newBuilder().addHeader("X-Platform", "Android").build()
            chain.proceed(request)
        })
        .addInterceptor(HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        })
        .build()
}
