package com.test.filmnet.search

import android.annotation.SuppressLint
import android.content.Context
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.test.filmnet.core.Utility
import com.test.filmnet.network.model.video.VideoModel
import io.reactivex.BackpressureStrategy
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import java.util.concurrent.TimeUnit

class SearchView(context: Context, searchCallback: SearchCallback, behaviorSubject: BehaviorSubject<String>) : LinearLayout(context) {


    private var editText: EditText? = null
    private var recyclerView: RecyclerView? = null
    private var mAdapter: VideoRVAdapter? = null



    interface SearchCallback{
        fun search(query: String)
        fun nextPage()
    }

    init {
        orientation = VERTICAL
        val margin = Utility.dpToPx(context, 16)

        editText = EditText(context).apply {
            hint = "جست و جو"
            setSingleLine()
            imeOptions = EditorInfo.IME_ACTION_SEARCH
            setOnEditorActionListener { v, actionId, event -> //Search action
                if (actionId == EditorInfo.IME_ACTION_SEARCH && !text.isNullOrEmpty()) {
                    searchCallback.search(text.toString())
                    return@setOnEditorActionListener true
                }
                false
            }
            addTextChangedListener(object : TextWatcher{
                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

                }

                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

                }

                override fun afterTextChanged(p0: Editable?) {
                    behaviorSubject.onNext(text.toString())
                }

            })
        }
        addView(
            editText,
            LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                Utility.dpToPx(context, 56)
            ).apply {
                setMargins(
                    margin,
                    margin,
                    margin,
                    margin
                )
            }
        )

        mAdapter = VideoRVAdapter(){
            searchCallback.nextPage()
        }
        recyclerView = RecyclerView(context).apply {

            clipChildren = false
            clipToPadding = false
            setPadding(
                margin/2,
                margin / 2,
                margin/2,
                margin / 2,
            )
            adapter = mAdapter
            layoutManager = GridLayoutManager(context, 2, GridLayoutManager.VERTICAL, false)
        }
        addView(
            recyclerView,
            LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT,
            ).apply {
            }
        )



    }

    @SuppressLint("NotifyDataSetChanged")
    fun setupView(videos: List<VideoModel>?) {
        if (videos.isNullOrEmpty().not()){
            mAdapter?.addAll(videos!!)
        }
    }

    fun clearList() {
        mAdapter?.clearList()
    }

    fun onDestroy(){

    }
}