package com.test.filmnet.search

import android.os.Bundle
import android.view.View
import com.test.filmnet.core.BaseFragment
import com.test.filmnet.search.viewmodel.SearchViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import org.koin.android.ext.android.inject
import java.util.concurrent.TimeUnit

class SearchFragment: BaseFragment() {

    private var searchView: SearchView? = null
    private val viewModel: SearchViewModel by inject()

    private val behaviorSubject = BehaviorSubject.create<String>()
    private val disposable = CompositeDisposable()

    override fun setView(): View? {
        searchView = SearchView(requireContext(), object : SearchView.SearchCallback{
            override fun search(query: String) {
                search(query)
            }

            override fun nextPage() {
                //get next page for current search
                viewModel.getNextPage()
            }
        }, behaviorSubject).apply {

        }
        return searchView
    }

    //Search new text with new list
    private fun search(it: String) {
        searchView?.clearList()
        viewModel.search(it)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.liveData.observe(viewLifecycleOwner) {
            searchView?.setupView(it)
        }
        //debounce for runtime search when user change its search text
        disposable.add(
            behaviorSubject
                .debounce(300, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe{
                    search(it)
                }
        )
    }

    override fun onDestroyView() {
        super.onDestroyView()
        searchView = null
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable.clear()
    }

}