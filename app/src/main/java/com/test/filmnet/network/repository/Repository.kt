package com.test.filmnet.network.repository

import com.test.filmnet.network.ApiService
import com.test.filmnet.network.model.Result
import com.test.filmnet.network.model.SearchData
import com.test.filmnet.network.model.video.VideoModel
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class Repository(private val apiService: ApiService) {

    private fun <T> schedule(observable: Single<T>): Single<T> {
        return observable
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

    }


    fun search(query: String): Single<Result<SearchData>>{
        return schedule(apiService.search(query))
    }

    fun getNextPageWithUrl(url: String): Single<Result<List<VideoModel>>>{
        return schedule(apiService.getNextPageWithUrl(url))
    }


}