package com.test.filmnet.search

import android.annotation.SuppressLint
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.test.filmnet.core.Utility
import com.test.filmnet.network.model.video.VideoModel

class VideoRVAdapter(val paginationCallback: () -> Unit): RecyclerView.Adapter<VideoRVAdapter.VideoRVViewHolder>() {

    class VideoRVViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(videoModel: VideoModel) {
            (itemView as VideoItemView).setupView(videoModel)
        }
    }

    val data = ArrayList<VideoModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VideoRVViewHolder {
        return VideoRVViewHolder(VideoItemView(parent.context).apply {
            layoutParams = RecyclerView.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            ).apply {
                setMargins(
                    Utility.dpToPx(context, 8),
                    Utility.dpToPx(context, 8),
                    Utility.dpToPx(context, 8),
                    Utility.dpToPx(context, 8)
                )
            }
        })
    }

    override fun onBindViewHolder(holder: VideoRVViewHolder, position: Int) {
        holder.bind(data[position])
        if (position >= data.size - 1){
            paginationCallback.invoke()
        }
    }

    override fun getItemCount(): Int {
        return data.size
    }

    @SuppressLint("NotifyDataSetChanged")
    fun clearList() {
        data.clear()
        notifyDataSetChanged()
    }

    @SuppressLint("NotifyDataSetChanged")
    fun addAll(videos: List<VideoModel>) {
        data.addAll(videos)
        notifyDataSetChanged()
    }

}