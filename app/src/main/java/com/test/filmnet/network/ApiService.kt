package com.test.filmnet.network

import com.test.filmnet.network.model.Result
import com.test.filmnet.network.model.SearchData
import com.test.filmnet.network.model.video.VideoModel
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.Url

interface ApiService {

    @GET("/search")
    fun search(@Query("query")query: String): Single<Result<SearchData>>

    @GET
    fun getNextPageWithUrl(@Url url: String): Single<Result<List<VideoModel>>>
}