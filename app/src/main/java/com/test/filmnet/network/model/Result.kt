package com.test.filmnet.network.model

import com.google.gson.annotations.SerializedName

data class Result<T>(
    @SerializedName("data")
    val data: T?,
    @SerializedName("meta")
    val meta: Meta
)

data class Meta(
    @SerializedName("total_items_count") val totalItemsCount: Int,
    @SerializedName("remaining_items_count") val remainingItemCount: Int,
    @SerializedName("next_url") val nextUrl: String?,
    @SerializedName("operation_result") val operationResult: String,
    @SerializedName("operation_result_code") val operationResultCode: Int,
    @SerializedName("display_message") val displayMessage: String,
    @SerializedName("server_date_time") val serverDateTime: String
)